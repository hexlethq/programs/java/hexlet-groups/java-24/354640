package exercise;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.in;

import java.util.List;
import java.util.Arrays;
import java.util.ArrayList;
import org.junit.jupiter.api.Test;

class AppTest {
    List<Integer> numbers;
    int num;

    @Test
    void testTake1() {

        numbers = Arrays.asList(1, 2, 3, 4);
        num = 2;
        List<Integer> result = App.take(numbers, num);
        assertThat(result).isEqualTo(Arrays.asList(1, 2));
    }

    @Test
        void testTake2() {

        numbers = Arrays.asList(7, 3, 10);
        num = 8;
        List<Integer> result = App.take(numbers, num);
        assertThat(result).isEqualTo(Arrays.asList(7, 3, 10));


        

    }
}
