package exercise.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.List;
import java.nio.file.Paths;
import java.nio.file.Files;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.apache.commons.lang3.ArrayUtils;

public class UsersServlet extends HttpServlet {

    @Override
    public void doGet(HttpServletRequest request,
                      HttpServletResponse response)
            throws IOException, ServletException {

        String pathInfo = request.getPathInfo();

        if (pathInfo == null) {
            showUsers(request, response);
            return;
        }

        String[] pathParts = pathInfo.split("/");
        String id = ArrayUtils.get(pathParts, 1, "");
        showUser(request, response, id);
    }

    private List getUsers() throws JsonProcessingException, IOException {
        // BEGIN
        ObjectMapper mapper = new ObjectMapper();
        String path = "src/main/resources/users.json";
        return mapper.readValue(new String(Files.readAllBytes(Paths.get(path))), List.class);

        // END
    }

    private void showUsers(HttpServletRequest request,
                           HttpServletResponse response)
            throws IOException {

        // BEGIN
        List<HashMap<String, String>> user = getUsers();
        StringBuilder table = new StringBuilder();
        table.append("<h1>Пользователи</h1> <table>");
        user.forEach(map -> {
            table.append("<tr>"
                    + "<td>"
                    + map.get("id")
                    + "</td>"
                    + "<td>"
                    + "<a href=\"/users/" + map.get("id") + "\">"
                    + map.get("firstName") + " " + map.get("lastName")
                    + "</a>"
                    + "</td>"
                    + "</tr>"
            );

        });

        table.append("</table>");
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        out.println(buildHTML(table.toString()));

        // END
    }

    private void showUser(HttpServletRequest request,
                          HttpServletResponse response,
                          String id)
            throws IOException {

        // BEGIN
        List<HashMap<String, String>> user = getUsers();
        StringBuilder table = new StringBuilder();


        user.forEach(map -> {
            if (map.get("id").equals(id)) {

                table.append("<h1>Данные пользователя</h1> <table>"
                        + "<tr>"
                        + "<td>id</td>"
                        + "<td>" + map.get("id") + "</td>"
                        + "</tr>"
                        + "<tr>"
                        + "<td>firstName</td>"
                        + "<td>" + map.get("firstName") + "</td>"
                        + "</tr>"
                        + "<tr>"
                        + "<td>lastName</td>"
                        + "<td>" + map.get("lastName") + "</td>"
                        + "</tr>"
                        + "<tr>"
                        + "<td>email</td>"
                        + "<td>" + map.get("email") + "</td>"
                        + "</tr>"
                        + "<table>"
                );

            }

        });
        if (table.length() == 0) {
            response.sendError(HttpServletResponse.SC_NOT_FOUND);
            return;
        }
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        out.println(buildHTML(table.toString()));

        // END
    }

    private static String buildHTML(String table) {
        final String head =
                "<head>"
                        + " <meta charset=\"UTF-8\">"
                        + " <title>All users</title> "
                        + "<link href=\"https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css "
                        + "rel=\"stylesheet\""
                        + " integrity=\"sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We "
                        + "crossorigin=\"anonymous\">"
                        + " </head>";

        String body =
                "<body> <div class=\"container\">"
                        + table
                        + " </div> </body>";

        return "<!DOCTYPE html>"
                + " <html lang=\"ru\"> "
                + head
                + body
                + "</html>";


    }
}
