package exercise;

import exercise.geometry.*;

public class App {
    // segment[0][0] = x1 segment[0][1] = y1
    // segment[1][0] = x2 segment[1][1] = y2
    private static boolean isXPositive(double[][] segment) {
        if (Point.getX(segment[0]) > Point.getX(segment[1])) {
           return true;
        }
        return false;
    }

    private static boolean isYPositive(double[][] segment) {
        if (Point.getY(segment[0]) > Point.getY(segment[1])) {
           return true;
        }
        return false;
    }

    public static double[] getMidpointOfSegment(double[][] segment) {
        double[] middlePoint = {(Point.getX(segment[1]) + Point.getX(segment[0])) / 2,
                                (Point.getY(segment[1]) + Point.getY(segment[0])) / 2};
        return middlePoint;
    }
    public static double[][] reverse(double[][] segment) {
        return  Segment.makeSegment(Segment.getEndPoint(segment), Segment.getBeginPoint(segment));
    }
    public static boolean isBelongToOneQuadrant(double[][] segment) {

        boolean isQuadrant = false;
        //квадрант x+ y+
        if (isXPositive(segment) && isYPositive(segment)) {
            isQuadrant = true;
        }
        //квадрант x- y+
        if (!isXPositive(segment) && isYPositive(segment)) {
            isQuadrant = true;
        }
        //квадрант x+ y-
        if (isXPositive(segment) && !isYPositive(segment)) {
            isQuadrant = true;
        }
        //квадрант x- y-
        if (!isXPositive(segment) && !isYPositive(segment)) {
            isQuadrant = true;
        }
        return isQuadrant;
    }
}