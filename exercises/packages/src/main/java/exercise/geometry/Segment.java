package exercise.geometry;

 public class Segment{

     public static double[][] makeSegment(double[] point1, double[] point2) {
         double[][] segment ={point1, point2};
         return segment;
     }

     public static double[] getBeginPoint(double[][] segment) {
         double[] startPoint = {segment[0][0], segment[0][1]};
         return startPoint;
     }
     public static double[] getEndPoint(double[][] segment) {
        double[] startPoint = {segment[1][0], segment[1][1]};
         return startPoint;
     }
}