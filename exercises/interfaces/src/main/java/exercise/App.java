package exercise;


import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

// BEGIN
public class App {
    public static List<String> buildAppartmentsList(List<Home> homes, int count) {

         return homes.stream()
                 .sorted(Comparator.comparing(Home::getArea))
                 .limit(count)
                 .map(Object::toString)
                 .collect(Collectors.toList());


    }
}
// END