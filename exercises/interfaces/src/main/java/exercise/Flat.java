package exercise;

// BEGIN
public class Flat implements Home {

    double area;
    double balconyArea;
    int floor;

    public Flat(double area, double balconyArea, int floor) {
        this.area = area;
        this.balconyArea = balconyArea;
        this.floor = floor;
    }

    @Override
    public double getArea() {
        return area + balconyArea;
    }

    @Override
    public int compareTo(Home another) {

        return (getArea() - another.getArea()) >= 0 ?
                (getArea() - another.getArea()) == 0 ?
                        0 : 1
                : -1;
    }

    @Override
    public String toString() {
        return String.format("Квартира площадью %s метров на %d этаже", getArea(), floor);
    }
}
// END
