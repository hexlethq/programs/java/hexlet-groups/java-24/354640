package exercise;

import java.util.stream.IntStream;

// BEGIN
public class ReversedSequence implements CharSequence {

    String sentence;

    public ReversedSequence(String sentence) {
        this.sentence = new StringBuilder(sentence).reverse().toString();
    }

    @Override
    public String toString() {
        return sentence;
    }

    @Override
    public int length() {
        return sentence.length();
    }

    @Override
    public char charAt(int index) {
        return sentence.charAt(index);
    }

    @Override
    public boolean isEmpty() {
        return CharSequence.super.isEmpty();
    }

    @Override
    public CharSequence subSequence(int start, int end) {
        return sentence.subSequence(start, end);
    }

    @Override
    public IntStream chars() {
        return CharSequence.super.chars();
    }

    @Override
    public IntStream codePoints() {
        return CharSequence.super.codePoints();
    }
}
// END
