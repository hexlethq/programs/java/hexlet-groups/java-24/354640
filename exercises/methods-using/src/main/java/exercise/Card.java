package exercise;

class Card {
    public static void printHiddenCard(String cardNumber, int starsCount) {
        // BEGIN
        String result = "";
        char[] card = cardNumber.toCharArray();
        for (int i = 0; i < starsCount; i++) {
            result = result + "*";
        }
        for (int i = 12; i < cardNumber.length(); i++) {
            result = result + card[i];
        }

        System.out.println(result);
        // END
    }
}
