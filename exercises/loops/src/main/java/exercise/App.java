package exercise;

class App {
    // BEGIN
    public static String getAbbreviation(String str) {
        String result = "";
        char[] chars = str.toCharArray();
        if (chars[0] != ' ') {
            result = result + Character.toUpperCase(chars[0]);
        }
        for (int i = 0; i < str.length(); i++) {
            if (chars[i] == ' ' && i != str.length() - 1) {
                result = result + Character.toUpperCase(chars[i + 1]);
            }
        }
        return result;
    }
    // END
}

