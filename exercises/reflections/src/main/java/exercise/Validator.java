package exercise;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// BEGIN
public class Validator {
    public static List<String> validate(Address address) {
        List<String> list = new ArrayList<>();
        try {
            Field[] fields = address.getClass().getDeclaredFields();

            for (Field field : fields) {
                field.setAccessible(true);
                if (field.isAnnotationPresent(NotNull.class) && field.get(address) == null) {
                    list.add(field.getName());
                }
            }
        } catch (IllegalAccessException e) {
            e.getMessage();
        }
        return list;

    }

    public static Map<String, List<String>> advancedValidate(Address address) {
        Map<String, List<String>> map = new HashMap<>();
        for (String field : validate(address)) {
            map.put(field, List.of("can not be null"));
        }

        try {
            Field[] fields = address.getClass().getDeclaredFields();

            for (Field field : fields) {
                field.setAccessible(true);
                MinLength minLength = field.getAnnotation(MinLength.class);
                if (field.get(address) != null
                        && field.isAnnotationPresent(MinLength.class)
                        && field.get(address).toString().length() < minLength.minLength()) {
                    map.put(field.getName(), List.of("length less than " + minLength.minLength()));
                }

            }

        } catch (IllegalAccessException e) {
            e.getMessage();
        }
        return map;
    }
}
// END
