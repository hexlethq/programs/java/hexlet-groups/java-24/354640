package exercise;

class App {
    // BEGIN
        public static int[] reverse(int[] number) {
            int lenArrNumber = number.length;
            int[] result = new int[lenArrNumber];
            for (int i = 0; i < lenArrNumber; i++) {
                result[lenArrNumber - (i + 1)] = number[i];
            }
            return result;
        }

        public static int getIndexOfMaxNegative(int[] number) {
           int index = -1;
           int var = 0;
           for (int i = 0; i < number.length; i++) {
               if (number[i] < 0 && var == 0) {
                   var = number[i];
                   index = i;
               }
               if (number[i] < 0 && number[i] > var) {
                   var = number[i];
                   index = i;
               }
            }
           return index;
        }
    // END
}
