package exercise;

import java.util.HashMap;
import java.util.Map;

// BEGIN
public class FileKV implements KeyValueStorage {

    private final String crsFile;

    public FileKV(String crsFile, Map<String, String> data) {
        Utils.writeFile(crsFile, Utils.serialize(data));
        this.crsFile = crsFile;
    }

    @Override
    public void set(String key, String value) {
        Map<String, String> kkk = Utils.unserialize(Utils.readFile(crsFile));
        kkk.put(key, value);
        Utils.writeFile(crsFile, Utils.serialize(kkk));
    }

    @Override
    public void unset(String key) {
        Map<String, String> kkk = Utils.unserialize(Utils.readFile(crsFile));
        kkk.remove(key);
        Utils.writeFile(crsFile, Utils.serialize(kkk));
    }

    @Override
    public String get(String key, String defaultValue) {
        return Utils.unserialize(Utils.readFile(crsFile)).getOrDefault(key, defaultValue);
    }

    @Override
    public Map<String, String> toMap() {
        return new HashMap<>(Utils.unserialize(Utils.readFile(crsFile)));
    }
}
// END
