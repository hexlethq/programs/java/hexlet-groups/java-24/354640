package exercise;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
//import java.util.Arrays;
import java.util.Locale;

class App {
    // BEGIN
    //создание констант тегов для html
    static final String START_UL = "<ul>";
    static final String END_UL = "</ul>";
    static final String START_LI = "<li>";
    static final String END_LI = "</li>";

    public static String buildList(String[] userList) {
        //проверка на пустой массив
        if (userList.length == 0) {
            return "";
        }
       //создание рабочей переменой и работа с ней
        StringBuilder userFile = new StringBuilder();
        userFile.append(START_UL).append("\n");
        //через цикл добавление текста полученного извне через массива
        for (int i = 0; i < userList.length; i++) {
            userFile.append("  ").append(START_LI).append(userList[i]).append(END_LI).append("\n");
        }
        userFile.append(END_UL);
        //преобразование в красивую строку
        String resultUserFile = userFile.toString();
        return resultUserFile;
    }
    //метод добавления одного элемента в массив
    //все просто, описание работы метода излишни
    public static String[] addArrayNow(String index, String[] userArray) {
        String[] thisUserArray = new String[userArray.length + 1];
        for (int i = 0; i < userArray.length; i++) {
            thisUserArray[i] = userArray[i];
        }
        thisUserArray[userArray.length] = index;
        return thisUserArray;
    }
    public static String getUsersByYear(String[][] userList, int date) {
        //создание рабочих переменных
        StringBuilder tempFile;
        String[] lastUserList = new String[0];
        //проверка на условие с добавлением "правильных" данных в массив
        for (int i = 0; i < userList.length; i++) {
            tempFile = new StringBuilder(userList[i][1]);
            if (Integer.parseInt(tempFile.substring(0, 4)) == date) {
                lastUserList = addArrayNow(userList[i][0], lastUserList);
            }
        }
        //возврат нужных данных в виде html страницы
        return buildList(lastUserList);
    }
    // END

    // Это дополнительная задача, которая выполняется по желанию.

    public static String getYoungestUser(String[][] users, String date) throws Exception {
        // BEGIN
        String userResult = "";
        //создаю два разных формата записи даты
        DateTimeFormatter jms = DateTimeFormatter.ofPattern("dd MMM yyyy", Locale.ENGLISH);
        DateTimeFormatter jms2 = DateTimeFormatter.ofPattern("yyyy-MM-dd", Locale.ENGLISH);
        //в "едином" формате дата создается переменные от пользователя и "нулевая"
        LocalDate dateUser = LocalDate.parse(date, jms);
        LocalDate tempUser = LocalDate.parse("1970-01-01", jms2);
        for (int i = 0; i < users.length; i++) {
            LocalDate dateList = LocalDate.parse(users[i][1], jms2);
            //если дата из файла раньше даты пользователя, заходим в условие
            if (dateList.isBefore(dateUser)) {
                //если искомая дата раньше чем прошедшая отбор
                if (tempUser.isBefore(dateList)) {
                    // меняем дату на более позднею
                    tempUser = dateList;
                    userResult = users[i][0];
                }
            }
        }
        return userResult;


        // END
    }
}
