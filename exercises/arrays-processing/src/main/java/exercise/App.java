package exercise;

class App {
    // BEGIN
    // высчитываем среднюю арифметическую сумму из чисел массива
    private static double sumMiddel(int[] arrInWork) {
        int sum = 0;
        for (int i : arrInWork) {
            sum = sum + i;
        }
        return sum / arrInWork.length;
    }
    // высчитываем размер массива при новых условиях
    private static int newSizeArr(int[] arrInWork, double sum) {
        int arrSize = 0;
        for (int i : arrInWork) {
            if (i <= sum) {
                arrSize++;
            }
        }
        return arrSize;
    }
    public static int[] getElementsLessAverage(int[] number) {
        if (number.length == 0) {
            return number;
        }
        double sum = sumMiddel(number);
        int[] newArr = new int[newSizeArr(number, sum)];
        int indexNewArr = 0;
        for (int i : number) {
            if (i <= sum) {
                newArr[indexNewArr] = i;
                indexNewArr++;
            }
        }
        return newArr;
    }

   // END
}
