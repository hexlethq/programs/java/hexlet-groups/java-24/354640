package exercise;

import java.util.Arrays;
import java.util.stream.Stream;

// BEGIN
public class App {


    public static String[][] enlargeArrayImage(String[][] image){

        return Arrays.stream(image)
                .flatMap(e -> Stream.of( Arrays.stream(e)
                                        .flatMap(f -> Stream.of(f, f))
                                        .toArray(String[]::new),

                                        Arrays.stream(e)
                                        .flatMap(f -> Stream.of(f, f))
                                        .toArray(String[]::new)))

                .toArray(String[][]::new);
    }

}

// END
