package exercise;

class App {
    public static boolean isBigOdd(int number) {
        // BEGIN
        boolean isBigOddVariable = false;
        if (number >= 1001 && number % 2 != 0) {
            isBigOddVariable = true;
        }
        // END
        return isBigOddVariable;
    }

    public static void sayEvenOrNot(int number) {
        // BEGIN
        if (number == 0) {
            System.out.println("0");
        }
        if (number % 2 == 0) {
            System.out.println("yes");
        }
        if (number % 2 != 0) {
            System.out.println("no");
        }

        // END
    }

    public static void printPartOfHour(int minutes) {
        // BEGIN
        if (minutes < 15) {
            System.out.println("First");
        }
        if (minutes >= 15 && minutes <= 30) {
            System.out.println("Second");
        }
        if (minutes >= 31 && minutes <= 45) {
            System.out.println("Third");
        }
        if (minutes >= 46 && minutes <= 59) {
            System.out.println("Fourth");
        }
        // END
    }
}
