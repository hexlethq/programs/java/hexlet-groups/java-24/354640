package exercise;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

// BEGIN
public class App {
    private static List<Character> addCharToList (String word) {
        List<Character> charToList = new ArrayList<>();
        for (char ch : word.toLowerCase(Locale.ROOT).toCharArray()) {
            charToList.add(ch);
        }

        return charToList;
    }
   public static boolean scrabble(String listCharInn, String word) {
       List<Character> checkListChar;
       List<Character> checkListWord;

       checkListChar = addCharToList(listCharInn);
       checkListWord = addCharToList(word);

       if (checkListWord.size() > checkListChar.size()) {
           return false;
       }

       boolean tempCheck = true;

       for (char ch : checkListWord) {
                 if (!checkListChar.contains(ch)) {
                      tempCheck = false;
                 }

                checkListChar.remove((Character)ch);
       }

        return tempCheck;
   }

}
//END
