package exercise;

class App {
    // BEGIN
        public static boolean searchForExistence(int side1, int side2, int side3) {
            if ((side1 + side2) > side3 && (side1 + side3) > side2 && (side2 + side3) > side1) {
                return true;
            }
            return false;
        }
        public static String getTypeOfTriangle(int side1, int side2, int side3) {
            if (!(searchForExistence(side1, side2, side3))) {
                return "Треугольник не существует";
            }
            if (side1 == side2 && side1 == side3) {
                return "Равносторонний";
            }
            if (side1 != side2 && side1 != side3 && side2 != side3) {
                return "Разносторонний";
            }
            return "Равнобедренный";
        }
    // END
}
