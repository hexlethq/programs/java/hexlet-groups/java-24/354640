package exercise;

public class Kennel {

    //создаем массив щенков
    private static String[][] listPuppy = new String[0][0];

    //метод добавления одной строки в общий массив щенков
    public static void addPuppy(String[] puppy) {

        //создание временного массива на одну строку больше
        String[][] newArr = new String[listPuppy.length + 1][2];

        //переписываем старую информацию
        int i = 0;
        for (String[] thePuppy : listPuppy) {
                 newArr[i] = thePuppy;
            i++;
        }

        //добавляем в последнюю строку новую информацию
        newArr[listPuppy.length] = puppy;

        //переопределяем ссылку на общий массив щенков
        listPuppy = newArr;
    }

        //метод добавления двойного массива в общий массив щенков
    public static void addSomePuppies(String[][] addArr) {
        for (int k = 0; k < addArr.length; k++) {
            addPuppy(addArr[k]);
        }
    }

    //возвращает количество строк общего массива щенков
    public static int getPuppyCount() {
        return listPuppy.length;
    }

    //возвращает true если имя щенка есть в общем массиве щенков
    //false если нет такого имени
    public static boolean isContainPuppy(String namePuppy) {
        for (String[] thePuppy : listPuppy) {
            if (thePuppy[0].equals(namePuppy)) {
                return true;
            }
        }
        return false;
    }

    //возвращает общий массив щенков
    public static String[][] getAllPuppies() {
        return listPuppy;
    }

    //возвращает список имен щенков по породе
    public static String[] getNamesByBreed(String breed) {
        //считаем размер нового массива
        //по совпадению породы
       int i = 0;
        for (String[] thePuppy : listPuppy) {
            if (thePuppy[1].equals(breed)) {
                 i++;
            }
        }

        //создаем массив
       String[] nameByBreed = new String[i];

        //добавляем в массив имена
        i = 0;
        for (String[] thePuppy : listPuppy) {
            if (thePuppy[1].equals(breed)) {
                nameByBreed[i] = thePuppy[0];
                i++;
            }
        }
        return nameByBreed;
    }

    //обнуляет общий массив щенков
    public static void resetKennel() {
        listPuppy = new String[0][0];
    }

    //удаление строки по индексу
    private static void removePuppyInListPuppy(int index) {
        //создаем массив на одну строку меньше
        String[][] newArr = new String[listPuppy.length - 1][2];
        //i - отсчет по индексу
        //j - шаг по новому массиву
        int i = 0;
        int j = 0;

        //заполнение нового массива
        for (String[] thePuppy : listPuppy) {
            if (i == index) {
                //когда достигаем нужного индекса
                //перескок итерации
                i++;
                continue;
            }

            newArr[j] = thePuppy;

            i++;
            j++;
        }

        listPuppy = newArr;
    }

    //метод нахождения щенка по имени и удаления
    //возвращает true
    //если нет такого щенка возвращает false
    public static boolean removePuppy(String namePuppy) {

        //по умолчанию щенка нет
        boolean isPuppyInListPuppy = false;

        //поиск совпадений по имени и фиксация индекса
        int index = 0;
        for (String[] thePuppy : listPuppy) {
            if (thePuppy[0].equals(namePuppy)) {
                //передача индекса в метод
                //для удаления строки по индексу
                removePuppyInListPuppy(index);
                isPuppyInListPuppy = true;
            }
            index++;
        }

        return isPuppyInListPuppy;
    }
}
