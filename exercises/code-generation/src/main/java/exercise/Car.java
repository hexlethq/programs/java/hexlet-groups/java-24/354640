package exercise;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.ObjectReader;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Value;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;


// BEGIN
@Getter
@Value
@AllArgsConstructor
// END
class Car {

    int id;
    String brand;
    String model;
    String color;
    User owner;

    // BEGIN
    static ObjectMapper mapper = new ObjectMapper();

    public String serialize() throws JsonProcessingException {
        return mapper.writeValueAsString(this);

    }

    public static Car unserialize(String json) throws IOException {
        Car car = mapper.readValue(json, Car.class);
        return car;
    }
    // END
}
