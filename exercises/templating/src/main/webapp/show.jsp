<%@ page import="java.util.Map" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!-- BEGIN -->
<% Map<String, String> user = (Map<String, String>) request.getAttribute("user");%>

<table cellspacing=5>

    <tr>
        <td>
            id
        </td>
        <td>
            <%= user.get("id")  %>
        </td>
    </tr>
    <tr>
        <td>
            firstName
        </td>
        <td>
            <%= user.get("firstName")  %>
        </td>
    </tr>
    <tr>
        <td>
            lastName
        </td>
        <td>
            <%= user.get("lastName")  %>
        </td>
    </tr>
    <tr>
        <td>
            email
        </td>
        <td>
            <%= user.get("email")  %>
        </td>
    </tr>

</table>


<button><a href="/users/delete?id=<%= user.get("id")%>">DELETE</a></button>

<!-- END -->
