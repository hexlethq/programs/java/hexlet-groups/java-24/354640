<%@ page import="java.util.Map" %>
<%@ page import="java.util.List" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!-- BEGIN -->
<% List<Map<String, String>> users = (List<Map<String, String>>) request.getAttribute("users");%>

<table cellspacing=5>
    <% for (int i = 0; i < users.size(); i++) {%>
    <tr>

        <td>
            <%= users.get(i).get("id")  %>
        </td>


        <td>
           <a href="/users/show?id=<%= users.get(i).get("id")%>">
               <%= users.get(i).get("firstName")  %> <%= users.get(i).get("lastName")  %>
           </a>
        </td>

    </tr>
    <%}%>
</table>
<!-- END -->
