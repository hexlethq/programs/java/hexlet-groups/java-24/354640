<%@ page import="java.util.Map" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!-- BEGIN -->
<% Map<String, String> user = (Map<String, String>) request.getAttribute("user");%>


<p>
    id = <%= user.get("id")%><br/>
    firstName = <%= user.get("firstName")%><br/>
    lastName = <%= user.get("lastName")%><br/>
    email = <%= user.get("email")%><br/>
</p>

<form action="/users/delete?id=<%= user.get("id")%>" method="post">
    <button type="submit" class="btn btn-danger">AGREE DELETE</button>
</form>

<!-- END -->
