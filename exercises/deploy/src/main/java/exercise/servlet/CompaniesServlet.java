package exercise.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.util.List;
import java.util.stream.Collectors;

import static exercise.Data.getCompanies;

public class CompaniesServlet extends HttpServlet {

    @Override
    public void doGet(HttpServletRequest request,
                      HttpServletResponse response)
            throws IOException, ServletException {

        // BEGIN
        PrintWriter writer = response.getWriter();
        String search = request.getParameter("search");
        List<String> companyList = getCompanies();



        if (search == null || search.equals("")) {
            writer.println(companyList.stream().collect(Collectors.joining(System.lineSeparator())));
        } else {
            String companyListAfterSearch = companyList.stream()
                    .filter(company -> company.contains(search))
                    .collect(Collectors.joining(System.lineSeparator()));

            if (companyListAfterSearch.isEmpty()) {
                writer.println("Companies not found");
            } else {
                writer.println(companyListAfterSearch);
            }

        }

        writer.close();
        // END
    }
}
