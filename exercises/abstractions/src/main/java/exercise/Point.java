package exercise;

class Point {
    // BEGIN
    public static int[] makePoint(int x, int y) {
        int[] coordinates = new int[2];
        coordinates[0] = x;
        coordinates[1] = y;
        return coordinates;
    }
    public static int getX(int[] coordinates) {
        return coordinates[0];
    }
    public static int getY(int[] coordinates) {
        return coordinates[1];
    }
    public static String pointToString(int[] coordinates) {
        String strCoordinates;
        strCoordinates = "(" + coordinates[0] + ", " + coordinates[1] + ")";
        return strCoordinates;
    }
    public static int getQuadrant(int[] coordinates) {
        if (coordinates[0] == 0 || coordinates[1] == 0) {
            return 0;
        }
        if (coordinates[0] > 0) {
            if (coordinates[1] > 0) {
                return 1;
            } else {
                return 4;
            }
        }
        if (coordinates[0] < 0) {
            if (coordinates[1] > 0) {
                return 2;
            } else {
                return 3;
            }
        }
        return 0;
    }
    public static int[] getSymmetricalPointByX(int[] coordinates) {
        coordinates[1] = -coordinates[1];
        return coordinates;
    }

    public static double calculateDistance(int[] coordinatesA, int[] coordinatesB) {
        return Math.sqrt(Math.pow((coordinatesA[0] - coordinatesB[0]), 2)
                + Math.pow((coordinatesA[1] - coordinatesB[1]), 2));
    }
    // END
}
