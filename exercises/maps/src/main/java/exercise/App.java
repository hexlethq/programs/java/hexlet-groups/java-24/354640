package exercise;

import java.util.HashMap;
import java.util.Map;

// BEGIN
public class App {
    public static HashMap getWordCount(String sentence) {
        String[] words = sentence.split(" ");
        HashMap<String, Integer> wordsCount = new HashMap<>();
        int val = 0;
        if (sentence.isEmpty()) {
            return wordsCount;
        }
        for (String word : words) {

            val = wordsCount.getOrDefault(word, 0) + 1;
            wordsCount.put(word, val);

        }

        return wordsCount;
    }

    public static String toString(HashMap<String, Integer> wordsCount) {
        if (wordsCount.isEmpty()) {
            return "{}";
        }
        StringBuilder result = new StringBuilder();
        result.append("{\n");
        for (Map.Entry<String, Integer> wordToCount : wordsCount.entrySet()) {
            result.append("  ")
                 .append(wordToCount.getKey())
                 .append(": ")
                 .append(wordToCount.getValue() + "\n");
        }
        result.append("}");

        return String.valueOf(result);
    }
}
//END
