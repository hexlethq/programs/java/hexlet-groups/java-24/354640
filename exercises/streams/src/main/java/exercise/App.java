package exercise;

import java.util.List;
import java.util.Arrays;

// BEGIN
public class App {
    public static int getCountOfFreeEmails(List<String> email) {
        int count =(int) email.stream()
                .filter(name -> name.contains("@gmail.com") || name.contains("@yandex.ru") || name.contains("@hotmail.com") )
                .count();
        return count;
    }
}
// END
