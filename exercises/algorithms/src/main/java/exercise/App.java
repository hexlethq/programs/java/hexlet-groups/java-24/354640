package exercise;

class App {
    // BEGIN
    //сортировка пузырями
    public static int[] sort(int[] massiv) {
        for (int i = 0; i < (massiv.length); i++) {
            for (int j = 0; j < (massiv.length - 1 - i); j++) {
                if (massiv[j] > massiv[j + 1]) {
                    massiv[j] = massiv[j] + massiv[j + 1];
                    massiv[j + 1] = massiv[j] - massiv[j + 1];
                    massiv[j] = massiv[j] - massiv[j + 1];
                }
            }
        }
        return massiv;
    }
    /*
    //сортировка выбором
    public static int[] sort(int[] massiv) {

       for (int i = 0; i < (massiv.length - 1); i++) {
           int minI = 0;
           int minPoint = massiv[i];
           for (int j = i; j < massiv.length; j++) {
                if (massiv[j] < minPoint) {
                    minPoint = massiv[j];
                    minI = j;
                }
            }
           massiv[minI] = massiv[i];
           massiv[i] = minPoint;
       }
       return massiv;
    }*/

    // END
}
