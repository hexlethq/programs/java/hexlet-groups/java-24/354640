package exercise;
import exercise.connections.Connection;
import exercise.connections.Disconnected;



// BEGIN
public class TcpConnection {

    Connection state;

    String ipAddress;
    String date;
    int port;

    public TcpConnection(String ipAddress, int port) {
        this.ipAddress = ipAddress;
        this.port = port;
        this.state = new Disconnected(this);
    }

    public Connection getState() {
        return state;
    }

    public void setState(Connection state) {
        this.state = state;
    }

    public void writeDate(String date) {
        this.date = date;
    }

    public String getCurrentState() {
        return state.getCurrentState();
    }

    public void connect() {
        state.connect();
    }

    public void disconnect() {
        state.disconnect();
    }

    public void write(String date) {
        state.write(date);
    }
}
// END
