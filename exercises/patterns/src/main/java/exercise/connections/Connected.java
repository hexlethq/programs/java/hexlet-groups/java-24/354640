package exercise.connections;

import exercise.TcpConnection;

// BEGIN
public class Connected implements Connection{

    private TcpConnection tcpConnection;

    public Connected(TcpConnection tcpConnection) {
        this.tcpConnection = tcpConnection;
    }

    @Override
    public String getCurrentState() {
        return "connected";
    }

    @Override
    public void connect() {
        System.out.println("Error");
    }

    @Override
    public void disconnect() {
        TcpConnection tCC = this.tcpConnection;
        tCC.setState(new Disconnected(tCC));
    }

    @Override
    public void write(String data) {
        tcpConnection.writeDate(data);
    }
}
// END
