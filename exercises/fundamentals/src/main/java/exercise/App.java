package exercise;



class App {
    public static void numbers() {
        // BEGIN
        int sum = 8 / 2;
        int ost = 100 % 3;
        System.out.println(sum + ost);
        // END
    }

    public static void strings() {
        String language = "Java";
        // BEGIN
        String sp = " ";
        language = language + sp + "works" + sp + "on" + sp + "JVM";
        System.out.println(language);
        // END
    }

    public static void converting() {
        Number soldiersCount = 300;
        String name = "spartans";
        // BEGIN
        System.out.println(soldiersCount + " " + name);
        // END
    }
}
