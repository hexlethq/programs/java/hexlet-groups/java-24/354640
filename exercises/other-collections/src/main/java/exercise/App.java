package exercise;


import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

// BEGIN
public class App {
    public static LinkedHashMap<String, String> genDiff(Map<String, Object> map1, Map<String, Object> map2) {
        LinkedHashMap<String, String> result = new LinkedHashMap<>();

        if (map1.isEmpty() && !map2.isEmpty()) {
            map2.forEach((kMap2, vMap2) -> {
                result.put(kMap2, "added");
            });
        }

        Map<String, Object> map3 = new HashMap<>();
        map3.putAll(map1);
        map3.putAll(map2);

        map3.forEach((k, v) -> {
            if (map1.containsKey(k) && map2.containsKey(k)) {
                if (map1.get(k).equals(map2.get(k))) {
                    result.put(k, "unchanged");
                } else {
                    result.put(k, "changed");
                }
            } else {
                if (map1.containsKey(k)) {
                    result.put(k, "deleted");
                } else {
                    result.put(k, "added");
                }

            }


        });


        return result;
    }


}
//END
