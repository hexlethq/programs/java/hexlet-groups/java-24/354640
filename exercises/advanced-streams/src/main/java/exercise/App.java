package exercise;


import java.util.stream.Collectors;
import java.util.Arrays;

// BEGIN
public class App {
    public static String getForwardedVariables(String json){
 //      return  Arrays.stream(Arrays.stream(json.split("\n"))
//                            .filter(t -> t.startsWith("environment"))
//                            .collect(Collectors.joining())
//                            .replace("\"", " " )
//                            .replace(",", " " )
//                            .split(" "))
//                            .filter(t -> t.startsWith("X_FORWARDED_"))
//                            .collect(Collectors.joining(","))
//                            .replace("X_FORWARDED_", "");

        return json.lines()
                .filter(t -> t.startsWith("environment"))
                .map(it -> it.replace("\"", " "))
                .map(it -> it.replace(",", " "))
                        .flatMap(it -> Arrays.stream(it.split(" ")))
                        .filter(t -> t.startsWith("X_FORWARDED_"))
                        .collect(Collectors.joining(","))
                        .replace("X_FORWARDED_", "");
    }

}
//END
