package exercise;

// BEGIN
public class NegativeRadiusException extends Exception {

    String sentence;

    public NegativeRadiusException(String sentence) {
        this.sentence = sentence;
    }

    @Override
    public String getMessage() {
        return sentence;
    }
}
// END
