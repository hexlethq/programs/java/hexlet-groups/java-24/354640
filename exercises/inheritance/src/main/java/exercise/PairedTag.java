package exercise;

import java.util.Map;
import java.util.List;
import java.util.stream.Collectors;

// BEGIN
public class PairedTag extends Tag {
    private final String nameTag;
    private Map<String, String> attributes;
    private final String bodyTag;
    private List<Tag> childrenOfTag;

    public PairedTag(String nameTag, Map<String, String> attributes, String bodyTag, List<Tag> childrenOfTag) {
        this.nameTag = nameTag;
        this.attributes = attributes;
        this.bodyTag = bodyTag;
        this.childrenOfTag = childrenOfTag;
    }


    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        childrenOfTag.forEach(t -> sb.append(t.toString()));
        return "<" + nameTag + createBodyTag(attributes) + ">" + bodyTag
                + sb + "</" + nameTag + ">" + "";

    }
}
// END
