package exercise;

import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;

// BEGIN
public class SingleTag extends Tag {

    private final String nameTag;
    private Map<String, String> attributes;

    public SingleTag(String nameTag, Map<String, String> attributes) {
        this.nameTag = nameTag;
        this.attributes = attributes;

    }

    @Override
    public String toString() {
        return "<" + nameTag + createBodyTag(attributes) + ">";
    }
}
// END
