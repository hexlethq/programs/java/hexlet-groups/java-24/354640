package exercise;

import java.util.stream.Collectors;
import java.util.Map;

// BEGIN
public class Tag {
    static String createBodyTag(Map<String, String> attributes) {
        StringBuilder ter = new StringBuilder();
        attributes.forEach((k, v) -> {
            ter.append(" ");
            ter.append(k + "=" + "\"" + v + "\"");
        });

        return ter.toString();
    }
}
// END
