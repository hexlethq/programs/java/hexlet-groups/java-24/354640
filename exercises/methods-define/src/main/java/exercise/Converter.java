package exercise;

class Converter {
    // BEGIN
    public static int convert(int number, String direction) {
        if (direction.equals("kb")) {
            return number / 1024;
        }
        if (direction.equals("b")) {
            return  number * 1024;
        }
        return  0;
    }
    public static void main(Object o) {
        System.out.println("10 Kb =" + " " + convert(10, "b") + " " + "b");

    }


    // END
}
