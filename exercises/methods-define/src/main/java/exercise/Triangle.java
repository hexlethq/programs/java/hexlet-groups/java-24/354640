package exercise;

class Triangle {
    // BEGIN
    public static double getSquare(int side1, int side2, int corner) {
        double cornerRad = (corner * Math.PI) / 180;
        return ((side1 * side2) / 2) * Math.sin(cornerRad);
    }
    public static void main(Object o) {
        System.out.println(getSquare(4, 5, 45));
    }
    // END
}
